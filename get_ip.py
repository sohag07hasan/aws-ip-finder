#!/usr/bin/env python
import requests, xlsxwriter, sys

try:
    aws = requests.get('https://ip-ranges.amazonaws.com/ip-ranges.json').json()
    ip_ranges = aws['prefixes']
    create_date = aws['createDate']
    amazon_ips = [item['ip_prefix'] for item in ip_ranges if item["service"] != ""]
    amazon_ips = list(set(amazon_ips)) #removing duplicate IPs

    #print(amazon_ips)

    #writing to file
    #filename = AWS + '_' + create_date + '.xlsx'
    filename = "aws_ip.xlsx"
    workbook = xlsxwriter.Workbook(filename)
    worksheet = workbook.add_worksheet('ipv4')

    for row, ip in enumerate(amazon_ips):
        worksheet.write(row, 0, str(ip))
    workbook.close()

    print(filename + ' created')

except:
    print("Unexpected error:", sys.exc_info()[0])
